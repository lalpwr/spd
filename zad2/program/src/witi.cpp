#include "../inc/witi.h"

#define BITSET 4

using namespace std;

void WITI::readData(string name)
{
	int n;
  int tmp1=0,tmp2=0,tmp3=0;
  ifstream file;
  file.open(name.c_str());
    if(file.good())
      {
	file >> n;
	
	for(int i = 0; i<n ; i++)
	  {
	 	   	file >> tmp1 >> tmp2 >> tmp3;
//			cout << tmp1 << tmp2 << tmp3 << endl;
	  	  	item tmp;
			tmp.P = tmp1;
			tmp.W = tmp2;
			tmp.D = tmp3;
			table.push_back(tmp);

	  }
        file.close();
      }

}

void WITI::showData(void)
{
	for(unsigned int i;i<table.size();i++)
		cout << table[i].P << " " << table[i].W << " " << table[i].D  << endl;
}

void WITI::showResults(void)
{

	for(unsigned int i=0;i<results.size();i++)
	{	cout.width( 2 );
		cout << i+1 << " ";}
	cout << endl;

	for(unsigned int i=0;i<results.size();i++)
	{	cout.width( 2 );
		cout << cMax(i) << " ";}
	cout << endl;

	for(unsigned int i=0;i<results.size();i++)
	{ 	cout.width( 2 );
		cout << results[i] << " ";}
	cout << endl;
}

int WITI::cMax(int n)
{
	c_max = 0;
	const int m = table.size();
	string bin;
	std::string binary = std::bitset<BITSET>(n).to_string(); //to binary

	for(int i=0;i<m;i++)
		if(binary[m-i-1]=='1')
			c_max += table[i].P;

	return c_max;
}

int WITI::witi(void)
{
	int res = 0;

	for(unsigned int i=0;i<pow(2,table.size());i++)
	{
		int tmp = 100;
		string bin = bitset<BITSET>(i).to_string();//uwzac na to 4
		for(unsigned int m=0;m<bin.size();m++)
		{
			if(i>0)
			{
				if(bin[bin.size()-m-1]=='1')
				{
					int g = results[i-m-1]+((cMax(i)-table[m].D)*table[m].W);
//					cout << g << " ";
					if(g < 0) g = 0;				
					if(g<tmp) tmp = g;
				}
			}	
			else
			{
				tmp = (cMax(i)-table[m].D)*table[m].W;
				if(tmp < 0) tmp = 0;
			}
		}	
//		cout << endl;
		results.push_back(tmp);
	}
//	cout << cMax(8)-table[3].D << endl;
	res = results[results.size()-1];
	return res;
}
