#ifndef _RPQ_H_
#define _RPQ_H_
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <unistd.h>
#include <algorithm>
#include <bitset>
#include <cmath>

struct item{
	int P;
	int W;
	int D;
};

class WITI
{
	int c_max;
	std::vector<item> table;
	std::vector<int> results;

public:

	void readData(std::string name);
	void showData(void);
	void showResults(void);
	int cMax(int n);
	int retD(int n);
	int retW(int n);
	int witi(void);
};

#endif
