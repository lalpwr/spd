#include "../inc/rpq.h"

using namespace std;

int RPQ::Schrage(void)
{
  	int t=0,tmp_id=0;
	vector<item> G;
	item e;

	while(G.size()!=0||N.size()!=0)
	{
	//		cout << mytab.size();
	  //cout<<"bsxsxsssla";
	  while(N.size())// && N[minR(N)].r <= t)
		{
			tmp_id = minR(N);
			e.id=N[tmp_id].id;
			e.r=N[tmp_id].r;
			e.p=N[tmp_id].p;
			e.q=N[tmp_id].q;
			G.push_back(e);
			N.erase(N.begin()+tmp_id);
			if(G.size()==0)
				t=N[minR(N)].r;
	//		cout << "test" << k << endl;		

//			cout << "tes" << endl;
//			showData(mytab);
//			showData(G);

		}
	//	sleep(1);
//	showData(P);
		tmp_id = maxQ(G);
	//		showData(G);
		e.id=G[tmp_id].id;
		e.r=G[tmp_id].r;
		e.p=G[tmp_id].p;
		e.q=G[tmp_id].q;
		G.erase(G.begin()+tmp_id);
		P.push_back(e);
//		k++;
//		cout << t << " " << e.q << " " << tmp_id << endl <<endl;
		t+=e.p;
		c_max=max(c_max,t+e.q);
		
	}
	return c_max;
}

int RPQ::minR(vector<item> mytab)//znajduje najmniejsza wartosc R i zwraca id
{
  //int tmp = mytab[0].r,id= mytab[0].id,index=0;
  int tmp = mytab[0].r,index=0;
    for(unsigned int i = 0; i < mytab.size(); i++)
    {
		if(tmp>mytab[i].r)
		{
			tmp=mytab[i].r;
			//id=i;		
			index=i;
		}			
    }
	return index;
}

int RPQ::maxQ(vector<item> mytab)//znajduje najwieksza wartosc R i zwraca id
{
  //int tmp = mytab[0].q,id=mytab[0].id,index=0;
  int tmp = mytab[0].q,index=0;
  for(unsigned int i = 0; i < mytab.size(); i++)
    {
		if(tmp<mytab[i].q)
		{
			tmp=mytab[i].q;
			//	id=i;
			index=i;
		}	
    }
	return index;
}

void RPQ::readData(string str)//wczytuje z pliku
{
  N.clear();
  P.clear();
  c_max=0;
  int tmp1=0,tmp2=0,tmp3=0;
  ifstream file;
  file.open(str.c_str());
    if(file.good())
      {
	file >> n >> tmp1;
	
	for(int i = 0; i<n ; i++)
	  {
	    file >> tmp1 >> tmp2 >> tmp3;
	    item tmp;
	    tmp.id = i;
			tmp.r = tmp1;
			tmp.p = tmp2;
			tmp.q = tmp3;
			tmp.t_on=0;
			N.push_back(tmp);
	  }
        file.close();
      }
    
    //cout<<"min to  "<< minR(N);
}

void RPQ::saveData(string str)//zapisuje do pliku
{
	ofstream file;
	file.open(str.c_str());
       file << 1 << " " << n << endl;

		for(int i = 0; i<n ; i++)
		{
			file << P[i].id+1 << " ";
		}
		file << endl << c_max;
        file.close();

}

void RPQ::showData(vector<item> mytab)//wyswietlanie N
{
    for(size_t i = 0; i < mytab.size(); ++i) 
		cout << mytab[i].id+1 << " " << mytab[i].r << " " << mytab[i].p << " " << mytab[i].q << endl;

}

int RPQ::count_c_max(){
  int q_tmp=0;
  c_max=0;
  
  for(int i = 0; i<n ; i++){
    while(c_max<P[i].r){
      c_max++;
      if(q_tmp>0) q_tmp--;
    }
    c_max+=P[i].p;
    q_tmp-=P[i].p;
    if(q_tmp<P[i].q) q_tmp=P[i].q;
  }
  c_max+=q_tmp;
  return c_max;
}


int RPQ::Schrage2(void)
{
  int t=0,tmp_id=0,tmp_p3=0;
  vector<item> G,tmp_N = N;
  vector<item>::iterator adress;
  item e;
  // cout<<"\n Nsize:"<<N[minR(N)].r; 
  while(G.size()!=0||N.size()!=0)
    {     
      while((N.size()!=0 && N[minR(N)].r <= t) || tmp_p3==1)
	{
	  tmp_p3=0;
	  tmp_id = minR(N);
	  e.id=N[tmp_id].id;
	  e.r=N[tmp_id].r;
	  e.p=N[tmp_id].p;
	  e.q=N[tmp_id].q;
	  G.push_back(e);
	  N.erase(N.begin()+tmp_id);
	  
	  if(G.size()==0){
	    t=N[minR(N)].r;
	    tmp_p3=1;
	  }
	  
	}
      
      if(G.size()==0){t++;}
      else{
	
	tmp_id = maxQ(G);
	//		showData(G);
	//adress = find_if(G.begin(), G.end(),find_id(tmp_id));
	e.id=G[tmp_id].id;
	e.r=G[tmp_id].r;
	e.p=G[tmp_id].p;
	e.q=G[tmp_id].q;
	
	//	cout<<e.id<<e.r<<e.p<<e.q;
	// adress = find_if(G.begin(), G.end(),find_id(tmp_id));
	//G.erase(adress);
	G.erase(G.begin()+tmp_id);
      P.push_back(e);
      
      t+=e.p;
 
      c_max=max(c_max,t+e.q);
      }
    }
  N=tmp_N;
  return c_max;
}

