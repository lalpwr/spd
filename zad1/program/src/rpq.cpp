#include "../inc/rpq.h"

using namespace std;

int RPQ::Schrage(void)
{
  	int t=0,tmp_id=0;
	vector<item> G;
	item e;

	while(G.size()!=0||N.size()!=0)
	{
	//		cout << mytab.size();
	  //cout<<"bsxsxsssla";
	  while(N.size())// && N[minR(N)].r <= t)
		{
			tmp_id = minR(N);
			e.id=N[tmp_id].id;
			e.r=N[tmp_id].r;
			e.p=N[tmp_id].p;
			e.q=N[tmp_id].q;
			G.push_back(e);
			N.erase(N.begin()+tmp_id);
			if(G.size()==0)
				t=N[minR(N)].r;
	//		cout << "test" << k << endl;		

//			cout << "tes" << endl;
//			showData(mytab);
//			showData(G);

		}
	//	sleep(1);
//	showData(P);
		tmp_id = maxQ(G);
	//		showData(G);
		e.id=G[tmp_id].id;
		e.r=G[tmp_id].r;
		e.p=G[tmp_id].p;
		e.q=G[tmp_id].q;
		G.erase(G.begin()+tmp_id);
		P.push_back(e);
//		k++;
//		cout << t << " " << e.q << " " << tmp_id << endl <<endl;
		t+=e.p;
		c_max=max(c_max,t+e.q);
		
	}
	return c_max;
}

int RPQ::minR(vector<item> mytab)//znajduje najmniejsza wartosc R i zwraca id
{
  //int tmp = mytab[0].r,id= mytab[0].id,index=0;
  int tmp = mytab[0].r,index=0;
    for(unsigned int i = 0; i < mytab.size(); i++)
    {
		if(tmp>mytab[i].r)
		{
			tmp=mytab[i].r;
			//id=i;		
			index=i;
		}			
    }
	return index;
}

int RPQ::maxQ(vector<item> mytab)//znajduje najwieksza wartosc R i zwraca id
{
  //int tmp = mytab[0].q,id=mytab[0].id,index=0;
  int tmp = mytab[0].q,index=0;
  for(unsigned int i = 0; i < mytab.size(); i++)
    {
		if(tmp<mytab[i].q)
		{
			tmp=mytab[i].q;
			//	id=i;
			index=i;
		}	
    }
	return index;
}

void RPQ::readData(string str)//wczytuje z pliku
{
  N.clear();
  P.clear();
  c_max=0;
  int tmp1=0,tmp2=0,tmp3=0;
  ifstream file;
  file.open(str.c_str());
    if(file.good())
      {
	file >> n >> tmp1;
	
	for(int i = 0; i<n ; i++)
	  {
	    file >> tmp1 >> tmp2 >> tmp3;
	    item tmp;
	    tmp.id = i;
			tmp.r = tmp1;
			tmp.p = tmp2;
			tmp.q = tmp3;
			tmp.t_on=0;
			N.push_back(tmp);
	  }
        file.close();
      }
    
    //cout<<"min to  "<< minR(N);
}

void RPQ::saveData(string str)//zapisuje do pliku
{
	ofstream file;
	file.open(str.c_str());
       file << 1 << " " << n << endl;

		for(int i = 0; i<n ; i++)
		{
			file << P[i].id+1 << " ";
		}
		file << endl << c_max;
        file.close();

}

void RPQ::showData(vector<item> mytab)//wyswietlanie N
{
    for(size_t i = 0; i < mytab.size(); ++i) 
		cout << mytab[i].id+1 << " " << mytab[i].r << " " << mytab[i].p << " " << mytab[i].q << endl;

}

int RPQ::count_c_max(){
  int q_tmp=0;
  c_max=0;
  
  for(int i = 0; i<n ; i++){
    while(c_max<P[i].r){
      c_max++;
      if(q_tmp>0) q_tmp--;
    }
    c_max+=P[i].p;
    q_tmp-=P[i].p;
    if(q_tmp<P[i].q) q_tmp=P[i].q;
  }
  c_max+=q_tmp;
  return c_max;
}


int RPQ::Schrage2(void)
{
  int t=0,tmp_id=0,tmp_p3=0;
  vector<item> G,tmp_N = N;
  vector<item>::iterator adress;
  item e;
  // cout<<"\n Nsize:"<<N[minR(N)].r;
  
  while(G.size()!=0||N.size()!=0)
    {
      
      while((N.size()!=0 && N[minR(N)].r <= t) || tmp_p3==1)
	{
	  tmp_p3=0;
	  tmp_id = minR(N);
	  e.id=N[tmp_id].id;
	  e.r=N[tmp_id].r;
	  e.p=N[tmp_id].p;
	  e.q=N[tmp_id].q;
	  G.push_back(e);
	  N.erase(N.begin()+tmp_id);
	  
	  if(G.size()==0){
	    t=N[minR(N)].r;
	    tmp_p3=1;
	  }
	  
	}
      
      if(G.size()==0){t++;}
      else{
	
	tmp_id = maxQ(G);
	//		showData(G);
	//adress = find_if(G.begin(), G.end(),find_id(tmp_id));
	e.id=G[tmp_id].id;
	e.r=G[tmp_id].r;
	e.p=G[tmp_id].p;
	e.q=G[tmp_id].q;
	
	//	cout<<e.id<<e.r<<e.p<<e.q;
	// adress = find_if(G.begin(), G.end(),find_id(tmp_id));
	//G.erase(adress);
	G.erase(G.begin()+tmp_id);
      P.push_back(e);
      
      t+=e.p;
 
      c_max=max(c_max,t+e.q);
      }
    }
  N=tmp_N;
  return c_max;
}

int Carlier(void)
{
return 0;
}


int RPQ::CriticalPatch(){

  int aMax=0;
  for (int i=0; i < n; i++) { 
    if (P[i].t_on+P[i].p+P[i].q > aMax){
      b=i;
      aMax=P[i].t_on+P[i].p+P[i].q;
    }
  }
  a=b;
  while ((a>=1) && (P[a-1].t_on+P[a-1].p >= P[a].r) ){
    a--;
  }
  return 0;
}




int RPQ::InterferenceTask(){
  int foundC = 0;
  for(int i=b-1; i>=a; i--)
    if ((foundC==0) && (P[i].q < P[b].q)) {
      foundC=1;
      c=i;
    }
  if (foundC==1)
    return 1;
  else
    return 0;
}


int RPQ::h(int aa, int bb){
  if ((aa<0) || (aa>bb) || (bb>(int)P.size())) {
    cout<<"błąd wywołania funkcji h(int, int)\n";
    return 0;
  }
  return (minR(aa,bb)+sumP(aa,bb)+minQ(aa,bb));
}



int RPQ::minR(int aa, int bb){

  int minr=P[a].r;
  for (int i=a; i<=b; i++) {
    if (P[i].r<minr)
      minr=P[i].r;
  }
  return minr;
  
} 

int RPQ::minQ(int aa, int bb){
  int minq=P[a].q;
  for (int i=a; i<=b; i++) {
    if (P[i].q<minq)
      minq=P[i].q;
  }
  return minq;
}

int RPQ::sumP(int aa, int bb){
  int sump=0;
  for (int i=a; i<=b; i++) {
    sump+=P[i].p;
  }
  return sump;
} 

int RPQ::minRBlock(){return minR(c+1,b);} 

int RPQ::minQBlock(){return minQ(c+1,b);}  

int RPQ::sumPBlock(){return sumP(c+1,b);} 

int RPQ::fastLB(){
  sLB=max(h(c+1,b), h(c,b));
  return sLB;
}  


void RPQ::EliminationTest(int UB){

  int tmp_h=h(c+1,b);
  for (int i=0; i< (int) P.size(); i++) {
    if ((i<a)||(i>b)) {
      if (P[i].p > UB - tmp_h) { //i należy do L
	if(P[i].r+P[i].p+sumPBlock()+P[b].q >= UB )
	  P[i].r=max(P[i].r, minRBlock() + sumPBlock());
	else if (minRBlock()+P[i].p + sumPBlock()+ P[i].q >=UB)
	  P[i].q=max(P[i].q,minQBlock()+sumPBlock());
      }
    }
  }
  
}


int RPQ::SchragePtm(){
  
  int t=0, Cmax=0;
  //int tmp_id=0,tmp_p3=0;
  vector<item> G, N_Schrage,N_ready,N_interrupt;
  vector<item> N_copy = N;
  sort(N_copy.begin(),N_copy.end(), item::compareR);
  
  vector<item>::iterator adress;
  // item e;
  // cout<<"\n Nsize:"<<N[minR(N)].r;
  
  while ((!N_copy.empty()) || (!N_ready.empty())) {
    while ((!N_copy.empty()) && (N_copy.back().r<=t)) {
      N_ready.push_back(N_copy.back()); 
      N_copy.pop_back();
    }

    make_heap (N_ready.begin(),N_ready.end(),item::compareQ);
    make_heap (N_interrupt.begin(),N_interrupt.end(),item::compareQ);
    sort_heap (N_ready.begin(),N_ready.end(),item::compareQ);
    if (N_ready.empty()) {
      t=N_copy.back().r;
    }
    
    else {
      // while (!listaPrzerwan.empty())
      //listaPrzerw.pop(); //czyszczenie listy przerwan
      
      N_interrupt.clear();
      for (int i=0; i< (int)N_copy.size();i++)
	if ((N_ready.front().p + t > N_copy[i].r)&&(N_ready.front().q < N_copy[i].q)){
	  N_interrupt.push_back(N_copy[i]);  
	  sort_heap (N_interrupt.begin(),N_interrupt.end(),item::compareQ);
	}
      
      if (N_interrupt.empty()) { 
	N_Schrage.push_back(N_ready.front()); //dodajesz zadanie do listy
	N_ready.erase(N_ready.begin()); //usuwa zadanie z puli dostepnych
	N_Schrage.back().t_on=t; //ustawia realny czas rozpoczecia realizacji zadania na maszy
	t+=N_Schrage.back().p; //aktualizuje czas
	sort_heap (N_ready.begin(),N_ready.end(),item::compareQ);
	if (t + N_Schrage.back().q > Cmax)
	  Cmax=t + N_Schrage.back().q;
	
      }
      
      else { 
	item z1 = N_ready.front();
	N_ready.erase(N_ready.begin());
	sort_heap (N_ready.begin(),N_ready.end(),item::compareQ);
	item z2=z1;
	z1.p=N_interrupt.front().r-t; //część zadania, która zrealizuje się do momentu przerwania.
	z2.p-=z1.p; //pozostała część zadania: od part2.p (==part1.p oryginalnego) odejmujesz część, która będzie zrealizowana
	N_ready.push_back(z2); //reszta realizowanego zadania trafia spowrotem do listy dostepnych zadan
	sort_heap (N_ready.begin(),N_ready.end(),item::compareQ);
	N_Schrage.push_back(z1);
	N_Schrage.back().t_on=t;
	t+=N_Schrage.back().p;
	if (t + N_Schrage.back().q > Cmax)
	  Cmax=t + N_Schrage.back().q;
      }
    }
  }
  // cout<<endl<<Cmax<<endl;
  P=N_Schrage;
	c_max = Cmax;
  return Cmax;
 
  
}
<<<<<<< HEAD
=======

int RPQ::Carlier(){

  int LB, UB;
  int foundOptimal = 0;
  RPQ Optimal, Actual=*this, rpq1, rpq2;
  sLB=1; //i tak jest zdejmowana odrazu
  
  vector<RPQ> P_Carliera;
  make_heap (P_Carliera.begin(),P_Carliera.end(),RPQ::comparesLB);
  cout<<"TEST_1"<<endl;
  // showData(N);
  UB=Schrage2();
  //showData(P);
  cout<<endl<<UB<<endl;
  LB=SchragePtm();
  P_Carliera.push_back(*this);
  sort_heap (P_Carliera.begin(),P_Carliera.end(),RPQ::comparesLB);
  Optimal=*this;
  while ((!P_Carliera.empty())){// && (!foundOptimal))) { 
    Actual=P_Carliera.front();
    P_Carliera.erase(P_Carliera.begin());
    if (Actual.SchragePtm() < UB){ //najlepszy możliwy jest lepszy od znalezionego UB - dzielimy problem
      Actual.Schrage2();
      if (Actual.c_max < UB) { //układamy wg. algorytmu Schrage. Jezeli znaleźliśmy lepsze rozwiazanie - zapisujemy je.
	UB=Actual.c_max;
	Optimal=Actual;
      }
       Actual.CriticalPatch();
     
      if(Actual.InterferenceTask()) {
	Actual.EliminationTest(UB);
	rpq1=rpq2=Actual;
       
      	rpq1.N[rpq1.c].r=rpq1.minRBlock()+rpq1.sumPBlock();
	rpq2.N[rpq2.c].q=rpq2.minQBlock()+rpq2.sumPBlock();
	rpq1.fastLB();
	rpq2.fastLB();
	P_Carliera.push_back(rpq1);
	P_Carliera.push_back(rpq2);
	sort_heap (P_Carliera.begin(),P_Carliera.end(),RPQ::comparesLB);
      }
      else {
	foundOptimal=1;
	cout<<"znaleziono lokalnie optymalne: "<< Actual.c_max<<endl;
      }
    }
  }
  
  
  return 0;
}
>>>>>>> 448a76819fce93886c8bcb9468f0b5a3cbe87e77
